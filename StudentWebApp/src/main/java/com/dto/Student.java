package com.dto;

public class Student {
	private int id;
	private String name;
	private String gender;
	private String email;
	private String password;
	private String department;
	
	public Student() {
		super();
	}

	public Student(int id, String name, String gender, String email, String password, String department) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.password = password;
		this.department = department;
	}

	public int getid() {
		return id;
	}
	public void setid(int id) {
		this.id = id;
	}

	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}

	public String getgender() {
		return gender;
	}
	public void setgender(String gender) {
		this.gender = gender;
	}

	public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.email = email;
	}

	public String getpassword() {
		return password;
	}
	public void setpassword(String password) {
		this.password = password;
	}
	
	public String getdepartment() {
		return password;
	}
	public void setdepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ",  gender=" + gender
				+ ", email=" + email + ", password=" + password + ", department=" + department + "]";
	}

}
